<?php

namespace Super\Infrastructure\Services;

class DateTimeFormatter
{
    public static function getCustomDateTimeFormat($datetime)
    {
        $months = [
            'Jan' => 'يناير',
            'Feb' => 'فبراير',
            'Mar' => 'مارس',
            'Apr' => 'أبريل',
            'May' => 'مايو',
            'Jun' => 'يونيو',
            'Jul' => 'يوليو',
            'Aug' => 'أغسطس',
            'Sep' => 'سبتمبر',
            'Oct' => 'أكتوبر',
            'Nov' => 'نوفمبر',
            'Dec' => 'ديسمبر'
        ];
        $amPm = [
            'am' => 'صباحاً',
            'pm' => 'مساءًا'
        ];
        if (! is_null($datetime)) {
            return $months[$datetime->format('M')] . $datetime->format(' d, Y h:i ') . $amPm[$datetime->format('a')];
        }
        return null;
    }
}
