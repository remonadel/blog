<?php
namespace Super\Infrastructure\Util;

class Help
{
    public $array;
    public $json;
    public $http;
    public $date;
    public $accents;

    public function __construct()
    {
        $this->array = new ArrayHelper();
        $this->json =  new JsonHelper();
        $this->http =  new HttpRequestHelper();
        $this->date =  new DateHelper();
        $this->accents =  new AccentsHelper();
        $this->jms = new JMSHelper();
    }
}
