<?php

namespace Super\Infrastructure\Persistence\Doctrine\Repositories;

use Doctrine\ORM\EntityManager;
use Super\Domain\Contracts\Repository\CategoryRepositoryInterface;
use Super\Domain\Entities\Category;

class CategoryRepository extends AbstractRepository implements CategoryRepositoryInterface
{
    /**
     * CategoryRepository constructor.
     * @param EntityManager $em
     * @param Category $entity
     */
    public function __construct(EntityManager $em, Category $entity)
    {
        parent::__construct($em, $entity);
    }
}
