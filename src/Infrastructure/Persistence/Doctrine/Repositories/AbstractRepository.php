<?php

namespace Super\Infrastructure\Persistence\Doctrine\Repositories;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use LaravelDoctrine\ORM\Serializers\Jsonable;

/**
 * Class AbsctractRepository
 * @package Infrastructure\Doctrine\Repositories\AbsctractRepository
 */
abstract class AbstractRepository
{
    use Jsonable;
    /**
     * @var
     */
    protected $entity;
    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var string
     */
    protected $entityNamespace;

    public function getProximaPericia($companyId)
    {
        $query = $this->em->createQueryBuilder();

        $atual = $query->select('count(u.id)')
            ->from($this->entityNamespace,'u')
            ->where("u.company = $companyId")
            ->getQuery()
            ->getSingleScalarResult();

        $proxima = $atual + 1;
        return $proxima;
    }

    public function getPaginatedData(QueryBuilder $query, $filter)
    {
        $page = 1;

        if(isset($filter['page'])){
            $page = $filter['page'];
        }

        if($page == 'all'){
            return $this->returnWithoutPagination($query);
        }

        $paginatedData = (new Paginator($query->getQuery(), $page, $filter['perPage']))->getData();

        $normalizedItems = [];

        foreach ($paginatedData['items'] as $page){

            if(is_array($page) && isset($page[0])){
                unset($page[0]);
                $normalizedItems[] = $page;
            }else{
                $normalizedItems[] = $page;
            }
        }

        unset($paginatedData['items']);
        $paginatedData['items'] = $normalizedItems;

        return $paginatedData;
    }

    public function returnWithoutPagination($query)
    {
        $result = $this->em->createQuery($query->getDQL());
        $result = $result->getResult();

        $normalizedItems = [];
        foreach ($result as $page){
            if(is_array($page) && isset($page[0])){
                unset($page[0]);
                $normalizedItems[] = $page;
            }else{
                $normalizedItems[] = $page;
            }
        }

        return $normalizedItems;
    }

    /**
     * AbsctractRepository constructor.
     * @param EntityManager $em
     * @param $entity
     */
    public function __construct(EntityManager $em, $entity)
    {
        $this->em = $em;
        $this->entity = $entity;
        $this->entityNamespace = get_class($entity);
    }

    /**
     * @param int $entityId
     * @return null|object
     */
    public function find( $entityId)
    {
        return $this->em->find($this->entityNamespace, $entityId);
    }

    /**
     * @return null|object
     */
    public function findByActive()
    {
        return $this->em->getRepository($this->entityNamespace)->findBy(['active'=>1]);
    }


    protected function getAllEntityFieldsNames()
    {
        return $this->em->getClassMetadata($this->entityNamespace)->getFieldNames();
    }

    protected function getAllEntityAssociationNames()
    {
        return $this->em->getClassMetadata($this->entityNamespace)->getAssociationNames();
    }

    private function paginated(QueryBuilder $query, $offset, $limit)
    {
        $page = ($offset <= 0 ) ? 1 : ($offset / $limit) + 1;

        $paginatedData = (new Paginator($query->getQuery(), $page, $limit))->getData();

        $normalizedItems = [];

        foreach ($paginatedData['items'] as $page){

            if(is_array($page) && isset($page[0])){
                unset($page[0]);
                $normalizedItems[] = $page;
            }else{
                $normalizedItems[] = $page;
            }
        }

        unset($paginatedData['items']);
        $paginatedData['items'] = $normalizedItems;

        return $paginatedData;
    }

    protected function filterByQuery(QueryBuilder $qb, $filters)
    {
        if (isset($filters['q']) && ! empty($filters['q']) && isset($filters['fieldsNames']) && ! empty($filters['fieldsNames'])) {
            $searchedQuery = $filters['q'];
            $fieldsNames = array_filter(explode(',', $filters['fieldsNames']));
            if (! empty($fieldsNames)) {
                $orX = $qb->expr()->orX();
                foreach ($fieldsNames as $key => $fieldName) {
                    if (in_array($fieldName, $this->getAllEntityFieldsNames())) {
                        $orX->add("{$qb->getAllAliases()[0]}.{$fieldName} LIKE :{$fieldName}{$key}");
                        $qb->setParameter($fieldName . $key, '%' . $searchedQuery . '%');
                    }
                }
                if ($orX->getParts()) {
                    $qb->andWhere($qb->expr()->orX($orX));
                }
            }
        }
        return $qb;
    }

    /**
     * @param array $filters
     * @param integer $offset
     * @param integer $limit
     * @param array $order
     * @return null|object
     */
    public function findAllByFiltersAndPagination($filters, $offset = 0, $limit = 25)
    {
        $qb = $this->em->createQueryBuilder();
        $qb->select('e')->from($this->entityNamespace , 'e');

        $this->filterByQuery($qb, $filters);

        if (! empty($filters)) {
            foreach ($filters as $key => $value) {
                if ((in_array($key, $this->getAllEntityFieldsNames()) || in_array($key, $this->getAllEntityAssociationNames())) && ! is_array($filters[$key])) {
                    $qb->andWhere("e.$key = :$key");
                    $qb->setParameter($key, $value);
                } elseif (in_array($key, $this->getAllEntityAssociationNames())) {
                    $qb->andWhere($qb->expr()->isMemberOf(":{$key}", "e.{$key}"))
                        ->setParameter($key, $value);
                }
            }
        }

        if (isset($filters['notEqual']) && ! empty($filters['notEqual'])) {
            foreach ($filters['notEqual'] as $key => $value) {
                $qb->andWhere($qb->expr()->notIn("e.$key", $value));
            }
        }

        if (isset($filters['notEqualAssociation']) && ! empty($filters['notEqualAssociation'])) {
            foreach ($filters['notEqualAssociation'] as $key => $value) {
                $qb->andWhere("e.$key != :$key")->setParameter(":$key", $value);
            }
        }

        if (isset($filters['hasDateRange']) && isset($filters['dateRange']['field']) && isset($filters['dateRange']['from']) && isset($filters['dateRange']['to'])) {
            $qb->andWhere("e.{$filters['dateRange']['field']} BETWEEN :from AND :to")
                ->setParameter('from', $filters['dateRange']['from'])
                ->setParameter('to', $filters['dateRange']['to']);
        }

        if (isset($filters['orderBy']) && !empty($filters['orderBy']) && isset($filters['order']) && !empty($filters['order'])) {
            $orderByAccepted = ['asc', 'ASC', 'DESC', 'desc'];
            if (in_array($filters['order'], $orderByAccepted) && in_array($filters['orderBy'], $this->getAllEntityFieldsNames())) {
                $qb->orderBy("e.{$filters['orderBy']}", $filters['order']);
            }
        } else {
            $qb->orderBy("e.id", 'ASC');
        }
//        echo $qb->getDQL();exit;

        $qb->setFirstResult($offset)->setMaxResults($limit);

        if (isset($filters['pagination']) && $filters['pagination'] === false) {
            return $qb->getQuery()->getResult();
        }
        return $this->paginated($qb, $offset, $limit);
    }

    /**
     * @param array $filters
     * @param integer $offset
     * @param integer $limit
     * @param array $order
     * @return null|object
     */
//    public function findAllByFiltersAndPagination($filters, $offset = 0, $limit = 25)
//    {
//        $order = [];
//
//        if (isset($filters['order_by']) && !empty($filters['order_by']) && isset($filters['order']) && !empty($filters['order'])) {
//            $order[$filters['order_by']] = $filters['order'];
//            unset($filters['order_by']);
//            unset($filters['order']);
//        }
//
//        return $this->em->getRepository($this->entityNamespace)->findBy($filters, $order, $limit, $offset);
//    }

    /**
     * @param $arrKeyValue
     * @return null|object
     */
    public function findBy($arrKeyValue)
    {
        return $this->em->getRepository($this->entityNamespace)->findOneBy($arrKeyValue);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->em->getRepository($this->entityNamespace)->findAll();
    }

    /**
     * @param $arrKeyValue
     * @return array
     */
    public function findAllBy($arrKeyValue)
    {
        return $this->em->getRepository($this->entityNamespace)->findBy($arrKeyValue);
    }

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return new $this->entityNamespace();
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        return $entity;
    }

    public function onlySave($entity)
    {
        $this->em->persist($entity);
        $this->em->flush();
        $this->em->clear();
    }

    /**
     * @param $entity
     * @param $arrAttributes
     * @return mixed
     */
    public function update($entity, $arrAttributes)
    {
//        dd($arrAttributes);
        $acceptedAttributes = [];
        foreach ($arrAttributes as $key => $value) {
            if (in_array($key, $this->getAllEntityFieldsNames()) ||
                in_array($key, $this->getAllEntityAssociationNames())) {
                $acceptedAttributes[$key] = $value;
            }
        }
//        dd($acceptedAttributes);
        $this->setAttributes($entity,$acceptedAttributes);
        $this->addRelatedEntity($entity,$acceptedAttributes);
        return $this->save($entity);
    }

    /**
     * @param $entity
     */
    public function delete($entity)
    {
        $this->em->remove($entity);
        $this->em->flush();
    }

    /**
     * @param $arrValue
     * @return mixed
     */
    public function deleteAll($arrValue)
    {
        $entitiesId = implode(",",$arrValue);
        $q = $this->em->createQuery("delete {$this->entityNamespace} u where u.id IN ($entitiesId)");
        return $q->execute();
    }

    /**
     * @param $entityId
     * @return mixed
     */
    public function deleteById($entityId)
    {
        $q = $this->em->createQuery("delete {$this->entityNamespace} u where u.id = $entityId");
        return $q->execute();
    }

    /**
     * @param $entityId
     * @return mixed
     */
    public function deleteByFieldAndId($field,$entityId)
    {
        $q = $this->em->createQuery("delete {$this->entityNamespace} u where u.$field = $entityId");
        return $q->execute();
    }

    /**
     * @param $arrKeyValue
     * @return mixed
     */
    public function deleteByArrKeyValue($arrKeyValue)
    {
        $condition = '';
        foreach($arrKeyValue as $key => $value){
            if($condition){
                $condition .= ' AND ';
            }
            $condition .= 'u.'.$key.' = \''.$value.'\'';
        }

        $q = $this->em->createQuery("delete {$this->entityNamespace} u where {$condition}");
        return $q->execute();
    }

    /**
     * @param $companyId
     * @return mixed
     */
    public function count($companyId)
    {
        $qb = $this->em->createQueryBuilder();
        return $qb
            ->select('count(u.id)')
            ->from($this->entityNamespace , 'u')
            ->where("u.company = $companyId")
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600)
            ->getSingleScalarResult();
    }

    /**
     * @param $entityId
     * @return null|object
     */
    public function findExternal($entityNamespace,$entityId)
    {
        $entityLoaded = $this->em->find($entityNamespace, $entityId);
        return $entityLoaded;
    }

    /**
     * @param null $arrAttributes
     * @return mixed
     */
    public function loadNew($arrAttributes=null)
    {
        $entity =  new $this->entityNamespace();
        if($arrAttributes){
            $this->setAttributes($entity,$arrAttributes);
            $this->addRelatedEntity($entity,$arrAttributes);
        }

        if(method_exists($entity,'setCreatedAt')){
            $entity->setCreatedAt(new \DateTime('now'));
        }

        return $entity;
    }

    /**
     * @param $entity
     * @param null $arrAttributes
     * @return mixed
     */
    public function load($entity, $arrAttributes=null)
    {
        if($arrAttributes){
            $this->setAttributes($entity,$arrAttributes);
            $this->loadRelatedEntity($entity,$arrAttributes);
        }

        return $entity;
    }

    /**
     * @param $entity
     * @param $arrAttributes
     */
    public function setAttributes($entity,$arrAttributes)
    {
        foreach($arrAttributes as $attr => $val){
            if(method_exists($entity, 'set'.$attr)){
                $param = new \ReflectionParameter(array(get_class($entity), 'set'.$attr), 0);

                if(!is_null($param->getClass())){
                    $entityClass = $param->getClass()->name;
                    $value = $this->findExternal($entityClass,$val);
                    $entity->{'set'.$attr}($value);
                }
                else{
                    $entity->{'set'.$attr}($val);
                }
            }
        }
    }

    /**
     * @param $entity
     * @param $arrAttributes
     */
    public function addRelatedEntity($entity,$arrAttributes)
    {
        foreach($arrAttributes as $parentAttribute => $values){
            if(is_array($values)) {
                foreach ($values as $val) {
                    if(method_exists($entity, 'add'.$parentAttribute)){

                        $param = new \ReflectionParameter(array($this->entityNamespace, 'add'.$parentAttribute), 0);

                        if(!is_null($param->getClass())){

                            $entityClass = $param->getClass()->name;
                            $newRelatedEntity  = new $entityClass;

                            $this->setAttributes($newRelatedEntity,$val);

                            if(method_exists($newRelatedEntity,'setCreatedAt')){
                                $newRelatedEntity->setCreatedAt(new \DateTime('now'));
                            }

                            $parentClassId  = get_class($entity);
                            $parentClassId = substr($parentClassId, strrpos($parentClassId, '\\') + 1);

                            $newRelatedEntity->{'set'.$parentClassId}($entity);

                            $entity->{'add'.$parentAttribute}($newRelatedEntity);
                        }
                    } elseif(method_exists($entity, 'addMultiple'.$parentAttribute)) {
                        $param = new \ReflectionParameter(array($this->entityNamespace, 'addMultiple'.$parentAttribute), 0);
                        if ( ! is_null($param->getClass())) {
                            $entityClass = $param->getClass()->name;
                            $allRefrencies = [];
                            foreach ($values as $value) {
                                $allRefrencies[] = $this->findExternal($entityClass,$value);
                            }
                            $entity->{'setMultiple'.$parentAttribute}($allRefrencies);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $entity
     * @param $arrAttributes
     */
    public function loadRelatedEntity($entity,$arrAttributes)
    {
        foreach($arrAttributes as $attr => $val){
            if(is_array($val)){
                foreach ($val as $id){
                    if(method_exists($entity, 'add'.$attr)){

                        $param = new \ReflectionParameter(array($this->entityNamespace, 'add'.$attr), 0);

                        if(!is_null($param->getClass())){
                            $entityClass = $param->getClass()->name;

                            $value = $this->findExternal($entityClass,$id);
                            $entity->{'add'.$attr}($value);
                        }
                    }
                }
            }
        }
    }


    /**
     * @param $arrKeyValue
     * @return mixed|null|object
     */
    public function firstOrNew($arrKeyValue)
    {
        $entity = $this->findBy($arrKeyValue);
        if($entity){
            return $entity;
        }

        return $this->loadNew($arrKeyValue);
    }

    /**
     * @param $arrKeyValue
     * @return mixed|null|object
     */
    public function firstOrCreate($arrKeyValue)
    {
        $entity = $this->findBy($arrKeyValue);
        if($entity){
            return $entity;
        }

        $entity = $this->loadNew($arrKeyValue);
        $this->save($entity);
        return $entity;
    }

    /**
     * @param $searchTerm
     * @return string
     */
    public function normalizeSearchTerm($searchTerm)
    {
        $term = '%'.preg_replace('/\s+/', '%', $searchTerm).'%';
        return $term;
    }

}
