<?php

namespace Super\Domain\Services;

use Super\Domain\Abstractions\AbstractDomainService;
use Super\Domain\Contracts\Repository\CategoryRepositoryInterface;
use Super\Domain\Contracts\Service\CategoryServiceInterface;

class CategoryService extends AbstractDomainService implements CategoryServiceInterface
{
    /**
     * CategoryService constructor.
     * @param CategoryRepositoryInterface $repositoryContract
     */
    function __construct(CategoryRepositoryInterface $repositoryContract)
    {
        parent::__construct($repositoryContract);
    }
}
