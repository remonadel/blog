<?php

namespace Super\Domain\Entities;

use Super\Application\Error\Error;
use Super\Domain\Entities\Traits\Activatable;
use Super\Domain\Entities\Traits\Creatable;
use Super\Domain\Entities\Traits\Identifiable;
use Super\Domain\Entities\Traits\Sluggable;
use Super\Domain\Entities\Traits\Updatable;

/**
 * Class Category
 * @package Domain\Entities
 */
class Category extends Entity
{
    use Identifiable, Creatable, Updatable, Activatable, Sluggable;

    const SLUG_FIELD_NAME = 'name';

    /**
     * @var string
     */
     private $name;


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        if (empty($name)) {
            throw new \Super\Application\Exceptions\InvalidArgumentException(Error::REQUIRED);
        }
        $this->name = $name;
        return $this;
    }
}
