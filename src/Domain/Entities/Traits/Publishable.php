<?php

namespace Super\Domain\Entities\Traits;

use Super\Domain\Entities\User;
use Infrastructure\Services\DateTimeFormatter;

/**
 * Trait Publishable
 * @package Super\Domain\Entities\Traits
 */
trait Publishable
{
    /**
     * @var boolean
     */
    protected $published = 0;

    /**
     * @var \DateTime
     */
    protected $publishedAt;

    /**
     * @var User
     */
    protected $publishedBy;

    /**
     * @return bool
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }

    public function getPublishedAtFormatted()
    {
        return DateTimeFormatter::getCustomDateTimeFormat($this->publishedAt);
    }

    /**
     * @param \DateTime $publishedAt
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;
    }

    /**
     * @return User
     */
    public function getPublishedBy()
    {
        return $this->publishedBy;
    }

    /**
     * @param User $publishedBy
     */
    public function setPublishedBy($publishedBy)
    {
        $this->publishedBy = $publishedBy;
    }
}
