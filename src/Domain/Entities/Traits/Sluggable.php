<?php

namespace Super\Domain\Entities\Traits;

trait Sluggable
{
    protected $slug;

    /**
     * @param $string
     */
    public function setSlug()
    {
        $this->slug = $this->generateSlugFromString($this->{self::SLUG_FIELD_NAME});
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    private function generateSlugFromString($string, $separator = '-')
    {
        if (is_null($string)) {
            return '';
        }
        $string = trim($string);
        $string = mb_strtolower($string, "UTF-8");;
        $string = preg_replace("/[^a-z0-9_\s-ءاأإآؤئبتثجحخدذرزسشصضطظعغفقكلمنهويةى]#u/", "", $string);
        $special = [
            '.',
            '!',
            '&quot;',
            "'",
            "_",
            "–",
            '"',
            "-",
            ':',
            ';',
        ];
        $string = str_replace($special, '', $string);
        $string = preg_replace("/[\s-]+/", " ", $string);
        $string = preg_replace("/[\s_]/", $separator, $string);
        return $string;
    }
}
