<?php

namespace Super\Domain\Entities\Traits;

class Timestamable
{
    /**
     * @var \DateTime
     */
    protected $createdAt = 'CURRENT_TIMESTAMP';

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}
