<?php

namespace Super\Domain\Entities\Traits;


use Infrastructure\Services\DateTimeFormatter;

trait Updatable
{
    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     *
     */
    public function setUpdatedAtValue()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getUpdatedAtFormatted()
    {
        if (property_exists($this, 'publishedAt')) {
            if (! is_null($this->publishedAt) && ! is_null($this->updatedAt)) {
                $publishedAt = new \DateTime($this->publishedAt->format(('Y-m-d H:i:s')));
                $updatedAt = new \DateTime($this->updatedAt->format(('Y-m-d H:i:s')));
                if ($updatedAt > $publishedAt) {
                    return DateTimeFormatter::getCustomDateTimeFormat($this->updatedAt);
                }
            }
        }
    }
}
