<?php

namespace Super\Domain\Entities\Traits;
use Super\Domain\Entities\User;


/**
 * Trait Blameable
 * @package Super\Domain\Entities\Traits
 */
trait Blameable
{
    /**
     * @var User
     */
    protected $createdBy;

    /**
     * Set createdBy
     *
     * @param User|null $createdBy
     * @return mixed
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
