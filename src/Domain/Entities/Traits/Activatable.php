<?php

namespace Super\Domain\Entities\Traits;

trait Activatable
{
    /**
     * @var boolean
     **/
    protected $active = 0;

    /**
     * @return bool
     */
    public function isActive()
    {
        return ($this->active === 1) ? true : false;
    }

    /**
     * @param bool $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }
}
