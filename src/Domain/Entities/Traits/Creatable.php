<?php

namespace Super\Domain\Entities\Traits;


trait Creatable
{
    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     *
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
