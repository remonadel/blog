<?php

namespace Super\Application\Services;

use Super\Domain\Contracts\Service\CategoryServiceInterface;

class CategoryService
{
    private $categoryService;

    public function __construct(CategoryServiceInterface $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function findAll()
    {
        return $this->categoryService->findAll();
    }

    public function findBy($arrKeyValue)
    {
        return $this->categoryService->findBy($arrKeyValue);
    }

    public function findAllFilteredByPagination($filters, $offset, $limit)
    {
        return $this->categoryService->findAllFilteredByPagination($filters, $offset, $limit);
    }

    public function find($groupId)
    {
        return $this->categoryService->find($groupId);
    }

    public function alreadyExists($parameter,$exceptionMessage)
    {
        return $this->categoryService->alreadyExists($parameter, $exceptionMessage);
    }

    public function create($post)
    {
        return $this->categoryService->create($post);
    }

    public function update($groupId, $post)
    {
        return $this->categoryService->update($groupId, $post);
    }

    public function delete($groupId)
    {
        return $this->categoryService->delete($groupId);
    }
}
