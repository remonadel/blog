<?php

namespace Super\Application\Exceptions;

class InvalidArgumentException extends \Exception
{
    public function getStatusCode()
    {
        return $this->getCode();
    }
}
