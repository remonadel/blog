## Installation

  - ``composer install``
  - ``copy .env.example to .env``
  - ``change database credentials``
  - ``php artisan doctrine:schema:create``
 
  
## Run Application

  
  - ``php artisan serve``
