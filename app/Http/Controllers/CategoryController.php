<?php

namespace App\Http\Controllers;

use Super\Application\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     * @param CategoryService $applicationService
     */
    public function __construct(CategoryService $applicationService)
    {
        parent::__construct($applicationService);
    }

    public function index(Request $request)
    {
        $filter = $request->except('exclude');

        $limit = 25;
        $offset = 0;

        if (isset($filter['limit']) && is_numeric($filter['limit'])) {
            $limit = $filter['limit'];
        }

        if (isset($filter['page']) && is_numeric($filter['page'])) {
            $offset = $filter['page'] <= 0 ? 0 : ($filter['page'] - 1) * $limit;
        }

        $data = $this->applicationService->findAllFilteredByPagination($filter, $offset, $limit);
        return $this->sendResponse($data);
    }
}
