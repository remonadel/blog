<?php

namespace App\Http\Controllers;

use App\Http\Response\JsonResponseDefault;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Super\Infrastructure\Util\JMSHelper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var
     */
    protected $applicationService;

    /**
     * Controller constructor.
     * @param $applicationService
     */
    public function __construct($applicationService)
    {
        $this->applicationService = $applicationService;
    }


    public function serialize($entity, $exclude = false)
    {
        $jmsHelper = new JMSHelper();
        $json = $jmsHelper->setSerializerConfiguration(base_path('./src/Infrastructure/Persistence/Doctrine/Serializations'))
            ->toJson($entity, $exclude);

        $array = json_decode($json,true);

        return $array;
    }

    protected function sendResponse($data, $exclude = false)
    {
        $data = $this->serialize($data, $exclude);
        return JsonResponseDefault::create(true, $data, 'success', 200);
    }
}
