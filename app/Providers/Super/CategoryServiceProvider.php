<?php

namespace App\Providers\Super;

use Illuminate\Support\ServiceProvider;

class CategoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Super\Domain\Contracts\Repository\CategoryRepositoryInterface', 'Super\Infrastructure\Persistence\Doctrine\Repositories\CategoryRepository');
        $this->app->bind('Super\Domain\Contracts\Service\CategoryServiceInterface', 'Super\Domain\Services\CategoryService');
    }
}
