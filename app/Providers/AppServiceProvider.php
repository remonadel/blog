<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $providersNamespace = 'App\\Providers\\Super\\';
        foreach (glob(__DIR__ . '/Super/*.php') as $file) {
            $provider = @explode('/Super/', explode('.php', $file)[0])[1];
            if ($provider) {
                app()->register($providersNamespace . $provider);
            }
        }
    }
}
